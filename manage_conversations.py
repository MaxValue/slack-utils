#!/usr/bin/env python3
# coding: utf-8
#

import argparse, logging, os, sys, json
import slack_utils as util

parser = argparse.ArgumentParser(argument_default=False, description="Edit one or more conversations.")
parser.add_argument("channels", nargs="?", default=False, help="The id of the conversations.")
parser.add_argument("--name", help="Rename the conversation to the given NAME.")
parser.add_argument("--purpose", help="Set the purpose of the conversation to the given string.")
parser.add_argument("--topic", help="Set the topic of the conversation to the given string.")
parser.add_argument("--archive", action="store_true", help="Archive the conversation.")
parser.add_argument("--unarchive", action="store_true", help="Unarchive the conversation.")
parser.add_argument("--verbose", "-v", action="count", default=0, help="Turn on verbose mode.")
parser.add_argument("--quiet", "-q", action="store_true", help="Only return data but no log messages.")
parser.add_argument("--log", help="Logfile path. If omitted, stdout is used.")
parser.add_argument("--debug", "-d", action="store_true", help="Log all messages including debug.")
parser.add_argument("--csv", action="store_true", help="Return data as CSV row.")
parser.add_argument("--no-headings", action="store_true", help="Do not print CSV header.")
parser.add_argument("--outfile", type=argparse.FileType("w", encoding="utf-8"), default=sys.stdout, help="The file in which to save the export data to.")
parser.add_argument("--user-token", default=os.environ.get("SLACK_USER_TOKEN", False), help="The user authorization token. An environment variable (SLACK_USER_TOKEN) is recommended though.")
parser.add_argument("--bot-token", default=os.environ.get("SLACK_BOT_TOKEN", False), help="The bot authorization token. An environment variable (SLACK_BOT_TOKEN) is recommended though.")
args = parser.parse_args()

if not args.user_token or not args.bot_token:
    parser.error("No token provided! Provide an env var (SLACK_USER_TOKEN or SLACK_BOT_TOKEN) or use --user-token / --bot-token")

if args.quiet:
    loglevel = 100
elif args.debug:
    loglevel = logging.DEBUG
elif args.verbose:
    loglevel = logging.INFO
else:
    loglevel = logging.WARNING

if args.log:
    logging.basicConfig(filename=args.log, filemode="a", level=loglevel)
else:
    logging.basicConfig(level=loglevel)

client = util.client(token=args.user_token)

if args.channels:
    channelObjs = util.get_channelObj(args.channels)
elif not sys.stdin.isatty():
    channelObjs = util.get_channelObj(sys.stdin)

for channelObj in channelObjs:
    channelID = channelObj["id"]
    logging.info(f"Managing channel {channelID!r}")
    if args.name:
        name = args.name
        if "*" in name:
            channelInfo = util.return_one(client.conversations_info, channel=channelID, key="channel")
            oldName = channelInfo["name"]
            name = name.replace("*", oldName)
        util.return_one(client.conversations_rename, channel=channelID, name=name)
    if args.purpose:
        util.return_one(client.conversations_setPurpose, channel=channelID, purpose=args.purpose)
    if args.topic:
        util.return_one(client.conversations_setTopic, channel=channelID, topic=args.topic)
    if args.archive:
        util.return_one(client.conversations_archive, channel=channelID)
    if args.unarchive:
        util.return_one(client.conversations_unarchive, channel=channelID)
