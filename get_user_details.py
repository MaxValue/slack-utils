#!/usr/bin/env python3
# coding: utf-8
#


import argparse, logging, requests, json, os, sys, re, jmespath
import slack_utils as util

parser = argparse.ArgumentParser(argument_default=False, description="Get all profile details of the given users.")
parser.add_argument("--handle", action="store_true", help="Return the user handle (useful for mentioning the user).")
parser.add_argument("--verbose", "-v", action="count", default=0, help="Turn on verbose mode.")
parser.add_argument("--quiet", "-q", action="store_true", help="Only return data but no log messages.")
parser.add_argument("--log", help="Logfile path. If omitted, stdout is used.")
parser.add_argument("--debug", "-d", action="store_true", help="Log all messages including debug.")
parser.add_argument("--csv", action="store_true", help="Return data as CSV row.")
parser.add_argument("--no-headings", action="store_true", help="Do not print CSV header.")
parser.add_argument("--include-fields", action="store_true", help="Include custom user profile fields.")
parser.add_argument("--brief", action="store_true", help="Only show small summary of information.")
parser.add_argument("--outfile", type=argparse.FileType("w", encoding="utf-8"), default=sys.stdout, help="The file in which to save the export data to.")
parser.add_argument("--user-token", default=os.environ.get("SLACK_USER_TOKEN", False), help="The user authorization token. An environment variable (SLACK_USER_TOKEN) is recommended though.")
parser.add_argument("--bot-token", default=os.environ.get("SLACK_BOT_TOKEN", False), help="The bot authorization token. An environment variable (SLACK_BOT_TOKEN) is recommended though.")
parser.add_argument("user", nargs="?", default=False, help="The targeted user.")
args = parser.parse_args()

if not args.user_token or not args.bot_token:
    parser.error("No token provided! Provide an env var (SLACK_USER_TOKEN or SLACK_BOT_TOKEN) or use --user-token / --bot-token")

if args.quiet:
    loglevel = 100
elif args.debug:
    loglevel = logging.DEBUG
elif args.verbose:
    loglevel = logging.INFO
else:
    loglevel = logging.WARNING

if args.log:
    logging.basicConfig(filename=args.log, filemode="a", level=loglevel)
else:
    logging.basicConfig(level=loglevel)

client = util.client(token=args.user_token, userinfo=True)

count_output = 0
if args.user:
    userObj = util.get_userObj(args.user)[0]
    userID = userObj["id"]
elif not sys.stdin.isatty():
    userObj = util.get_userObj(sys.stdin.readline())[0]
    userID = userObj["id"]
else:
    userObj = util._userInfo
    userID = userObj.get("id", "")

if util.re_userID.match(userID):
    result = util.return_one(client.users_info, "user", user=userID)
    if args.handle:
        handle_name = util.jmes(
            path="profile.display_name",
            dataObj=result,
            default=util.jmes("profile.real_name", result)
        )
        print(f"@{handle_name}")
    else:
        if args.include_fields:
            result["profile"] = util.return_one(client.users_profile_get, "profile", user=userID)
        if args.csv:
            if count_output == 0 and not args.no_headings:
                util.write_csv_heading(
                    fieldmap=util.FIELDMAP_USER,
                    file=args.outfile
                )
            util.write_csv(
                fieldmap=util.FIELDMAP_USER,
                dataObj=result,
                file=args.outfile
            )
        elif args.brief:
            userType = "member"
            if result.get("is_primary_owner", False):
                userType = "owner primary"
            elif result.get("is_owner", False):
                userType = "owner"
            elif result.get("is_admin", False):
                userType = "admin"
            elif result.get("is_bot", False):
                userType = "bot"
            elif result.get("is_app_user", False):
                userType = "app"
            elif result.get("is_ultra_restricted", False):
                userType = "guest singlechannel"
            elif result.get("is_restricted", False):
                userType = "guest"
            userStatus = "active"
            if result.get("deleted", False):
                userStatus = "deactivated"
            print(f"TEAM ID:\t\t{util.jmes('team_id', result)}")
            print(f"TEAM NAME:\t\t{util._teamInfo.get('name', '')}")
            print(f"TEAM DOMAIN:\t\t{util._teamInfo.get('domain', '')}")
            print(f"ACCOUNT ID:\t\t{util.jmes('id', result)}")
            print(f"ACCOUNT TYPE:\t\t{userType}")
            print(f"ACCOUNT STATUS:\t\t{userStatus}")
            print(f"ACCOUNT NAME FIRST:\t{util.jmes('profile.first_name', result)}")
            print(f"ACCOUNT NAME LAST:\t{util.jmes('profile.last_name', result)}")
            print(f"ACCOUNT EMAIL:\t\t{util.jmes('profile.email', result, '[MISSING SCOPE: users:read.email]')}")
        else:
            print(result)
    count_output += 1
else:
    logging.error(f"{userID} is not a valid user id!")
