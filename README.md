# Slack Utilities

Various Python scripts which help you to quickly interact with Slack workspaces.

## Contents
* [Getting Started](#getting-started)
*    [Prerequisites](#prerequisites)
*    [Installing](#installing)
* [Running the tests](#running-the-tests)
* [Deployment](#deployment)
* [Built With](#built-with)
* [Contributing](#contributing)
*    [Roadmap](#roadmap)
* [Versioning](#versioning)
* [Authors](#authors)
* [License](#license)
* [Acknowledgments](#acknowledgments)
* [Project History](#project-history)

## Getting Started

Just install the prerequisites listed down below

### Prerequisites

* [Python 3.7 (minimum)](https://www.python.org/downloads/)
* A slack app ([create one here](https://api.slack.com/apps/new))

### Installation

Clone the repo to you computer
```
git clone git@gitlab.com:maxvalue/slack-utils.git
```

Run this command to install the virtual environment
```
./setup_environment.sh
```

Get yourself a Slack token: https://api.slack.com/apps/new

Make an environment file
```
cp .env.tpl myprettyslackworkspace.env
```

Fill in the environment file with your Slack token
```
vim myprettyslackworkspace.env
```

Activate the environment
```
source activate_environment.sh myprettyslackworkspace.env
```

Run the script you want to use
```
./SCRIPTNAME.py --help
```

To deactivate the virtual environment (removes the links to the Python environment):
```
deactivate
```

## Built With

* [Sublime Text 3](https://www.sublimetext.com/) - The code editor I use
* [Python 3](https://www.sublimetext.com/) - For executing the scripts
* [Mozilla Firefox](https://firefox.com/) - For adjusting parameters for the app

## Contributing

Please open an issue if you want to help.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/USERNAME/your/project/-/tags).

## Authors

* **Max Fuxjäger** - *Initial work* - [MaxValue](https://gitlab.com/MaxValue/)

## License

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details.

## Project History
This project was created because I (Max) needed some quick tools to administer some Slack workspaces.
