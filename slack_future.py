#!/usr/bin/env python3
# coding: utf-8

import slacker

"""
URL
    parameters
    return types
    errors
"""

class SlackData(object):
    pass

class Conversation(SlackData):
    def fromJSON(self, obj):
        self._obj = obj
        self.created = obj.get("created", None)
        self.creator = obj.get("creator", None)
        self.id = obj.get("id", None)
        self.is_archived = obj.get("is_archived", None)
        self.is_channel = obj.get("is_channel", None)
        self.is_ext_shared = obj.get("is_ext_shared", None)
        self.is_general = obj.get("is_general", None)
        self.is_group = obj.get("is_group", None)
        self.is_im = obj.get("is_im", None)
        self.is_member = obj.get("is_member", None)
        self.is_mpim = obj.get("is_mpim", None)
        self.is_org_shared = obj.get("is_org_shared", None)
        self.is_pending_ext_shared = obj.get("is_pending_ext_shared", None)
        self.is_private = obj.get("is_private", None)
        self.is_read_only = obj.get("is_read_only", None)
        self.is_shared = obj.get("is_shared", None)
        self.last_read = obj.get("last_read", None)
        self.locale = obj.get("locale", None)
        self.name = obj.get("name", None)
        self.name_normalized = obj.get("name_normalized", None)
        self.num_members = obj.get("num_members", None)
        self.pending_shared = obj.get("pending_shared", None)
        self.previous_names = obj.get("previous_names", None)
        self.purpose = obj.get("purpose", None)
        self.topic = obj.get("topic", None)
        self.unlinked = obj.get("unlinked", None)

class Channel(SlackData):
    def fromID(self, channelID):
        self.id = channelID
        # TODO call to get channel data

    def fromJSON(self, obj):
        self._obj = obj
        self.created = obj.get("created", None)
        self.creator = obj.get("creator", None)
        self.id = obj.get("id", None)
        self.is_archived = obj.get("is_archived", None)
        self.is_channel = obj.get("is_channel", None)
        self.is_general = obj.get("is_general", None)
        self.is_member = obj.get("is_member", None)
        self.is_mpim = obj.get("is_mpim", None)
        self.is_org_shared = obj.get("is_org_shared", None)
        self.is_private = obj.get("is_private", None)
        self.is_shared = obj.get("is_shared", None)
        self.last_read = obj.get("last_read", None)
        self.name = obj.get("name", None)
        self.name_normalized = obj.get("name_normalized", None)
        self.latest = obj.get("latest", None)
        self.unread_count = obj.get("unread_count", None)
        self.unread_count_display = obj.get("unread_count_display", None)
        self.members = obj.get("members", None)
        self.previous_names = obj.get("previous_names", None)
        self.purpose = obj.get("purpose", None)
        self.topic = obj.get("topic", None)

class ChannelList(list):
    def fromJSON(self, obj):
        self._channels = []
        if type(obj) == list:
            for channelID in obj:
                if type(channelID) == str:
                    self._channels.append(Channel().fromID(channelID))
                else:
                    raise TypeError("Given channel list does not only contain channel IDs.")
        elif type(obj) == dict:
            for channelID in obj.get("channels", []):
                if type(channelID) == str:
                    self._channels.append(Channel().fromID(channelID))
                else:
                    raise TypeError("Given channel list does not only contain channel IDs.")
        else:
            raise NotImplemented("Cannot use passed data type as basis for ChannelList")

class User(SlackData):
    def fromJSON(self, obj):
        self.obj = obj
        self.id = obj.get("id", None)
        self.team_id = obj.get("team_id", None)
        self.name = obj.get("name", None)
        self.deleted = obj.get("deleted", None)
        self.color = obj.get("color", None)
        self.real_name = obj.get("real_name", None)
        self.tz = obj.get("tz", None)
        self.tz_label = obj.get("tz_label", None)
        self.tz_offset = obj.get("tz_offset", None)
        self.profile = UserProfile(obj.get("profile", {}))
        self.is_admin = obj.get("is_admin", None)
        self.is_owner = obj.get("is_owner", None)
        self.is_primary_owner = obj.get("is_primary_owner", None)
        self.is_restricted = obj.get("is_restricted", None)
        self.is_ultra_restricted = obj.get("is_ultra_restricted", None)
        self.is_bot = obj.get("is_bot", None)
        self.is_stranger = obj.get("is_stranger", None)
        self.updated = obj.get("updated", None)
        self.is_app_user = obj.get("is_app_user", None)
        self.is_invited_user = obj.get("is_invited_user", None)
        self.has_2fa = obj.get("has_2fa", None)
        self.locale = obj.get("locale", None)

class UserProfile(SlackData):
    def fromJSON(self, obj):
        self.obj = obj
        self.title = obj.get("title", None)
        self.phone = obj.get("phone", None)
        self.skype = obj.get("skype", None)
        self.real_name = obj.get("real_name", None)
        self.real_name_normalized = obj.get("real_name_normalized", None)
        self.display_name = obj.get("display_name", None)
        self.display_name_normalized = obj.get("display_name_normalized", None)
        self.status_text = obj.get("status_text", None)
        self.status_emoji = obj.get("status_emoji", None)
        self.status_expiration = obj.get("status_expiration", None)
        self.avatar_hash = obj.get("avatar_hash", None)
        self.first_name = obj.get("first_name", None)
        self.last_name = obj.get("last_name", None)
        self.email = obj.get("email", None)
        self.image_original = obj.get("image_original", None)
        self.image_24 = obj.get("image_24", None)
        self.image_32 = obj.get("image_32", None)
        self.image_48 = obj.get("image_48", None)
        self.image_72 = obj.get("image_72", None)
        self.image_192 = obj.get("image_192", None)
        self.image_512 = obj.get("image_512", None)
        self.team = obj.get("team", None)
        self.fields = UserProfileFields.get("fields", [])

class File(SlackData):
    def fromJSON(self, obj):
        self.id = ob.get("id", None)
        self.created = ob.get("created", None)
        self.timestamp = ob.get("timestamp", None)
        self.name = ob.get("name", None)
        self.title = ob.get("title", None)
        self.mimetype = ob.get("mimetype", None)
        self.filetype = ob.get("filetype", None)
        self.pretty_type = ob.get("pretty_type", None)
        self.user = ob.get("user", None)
        self.editable = ob.get("editable", None)
        self.size = ob.get("size", None)
        self.mode = ob.get("mode", None)
        self.is_external = ob.get("is_external", None)
        self.external_type = ob.get("external_type", None)
        self.is_public = ob.get("is_public", None)
        self.public_url_shared = ob.get("public_url_shared", None)
        self.display_as_bot = ob.get("display_as_bot", None)
        self.username = ob.get("username", None)
        self.url_private = ob.get("url_private", None)
        self.url_private_download = ob.get("url_private_download", None)
        self.thumb_64 = ob.get("thumb_64", None)
        self.thumb_80 = ob.get("thumb_80", None)
        self.thumb_360 = ob.get("thumb_360", None)
        self.thumb_360_w = ob.get("thumb_360_w", None)
        self.thumb_360_h = ob.get("thumb_360_h", None)
        self.thumb_160 = ob.get("thumb_160", None)
        self.thumb_360_gif = ob.get("thumb_360_gif", None)
        self.image_exif_rotation = ob.get("image_exif_rotation", None)
        self.original_w = ob.get("original_w", None)
        self.original_h = ob.get("original_h", None)
        self.deanimate_gif = ob.get("deanimate_gif", None)
        self.pjpeg = ob.get("pjpeg", None)
        self.permalink = ob.get("permalink", None)
        self.permalink_public = ob.get("permalink_public", None)
        self.comments_count = ob.get("comments_count", None)
        self.is_starred = ob.get("is_starred", None)
        self.shares = ob.get("shares", None)
        self.channels = ob.get("channels", None)
        self.groups = ob.get("groups", None)
        self.ims = ob.get("ims", None)
        self.has_rich_preview = ob.get("has_rich_preview", None)

class Group(SlackData):
    ...

class MPIM(SlackData):
    ...

class IM(SlackData):
    ...

class Usergroup(SlackData):
    ...

class Event(SlackData):
    ...
