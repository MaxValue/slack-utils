#!/usr/bin/env python3
# coding: utf-8

import argparse, logging, json, os
import slack_utils as util

parser = argparse.ArgumentParser(argument_default=False, description="Does something with a slack workspace.")
parser.add_argument("--verbose", "-v", action="count", default=0, help="Turn on verbose mode.")
parser.add_argument("--quiet", "-q", action="store_true", help="Only return data but no log messages.")
parser.add_argument("--log", help="Logfile path. If omitted, stdout is used.")
parser.add_argument("--debug", "-d", action="store_true", help="Log all messages including debug.")
parser.add_argument("--csv", action="store_true", help="Return data as CSV row.")
parser.add_argument("--no-headings", action="store_true", help="Do not print CSV header.")
parser.add_argument("--outfile", type=argparse.FileType("w", encoding="utf-8"), default=sys.stdout, help="The file in which to save the export data to.")
parser.add_argument("--user-token", "-t", default=os.environ.get("SLACK_USER_TOKEN", False), help="The user authorization token. An environment variable (SLACK_USER_TOKEN) is recommended though.")
parser.add_argument("--bot-token", "-t", default=os.environ.get("SLACK_BOT_TOKEN", False), help="The bot authorization token. An environment variable (SLACK_BOT_TOKEN) is recommended though.")
parser.add_argument("objects", nargs="*", default=sys.stdin, help="The targeted objects.")
args = parser.parse_args()

if not args.user_token or not args.bot_token:
    parser.error("No token provided! Provide an env var (SLACK_USER_TOKEN or SLACK_BOT_TOKEN) or use --user-token / --bot-token")

if args.quiet:
    loglevel = 100
elif args.debug:
    loglevel = logging.DEBUG
elif args.verbose:
    loglevel = logging.INFO
else:
    loglevel = logging.WARNING

if args.log:
    logging.basicConfig(filename=args.log, filemode="a", level=loglevel)
else:
    logging.basicConfig(level=loglevel)

client = util.client(token=args.user_token)

count_output = 0
try:
    for dataObject in args.objects:
        try:
            dataObj = json.loads(dataObject)
            if type(dataObj) == str:
                objectID = dataObj
            else:
                objectID = dataObj.get("id", "")
        except json.decoder.JSONDecodeError:
            objectID = dataObject.strip()
            dataObj = {
                "id": objectID
            }
        if util.re_objectID.match(objectID):
            result = util.return_one(client.webapimethod, "datakey", object=objectID)
            if args.csv:
                if count_output == 0 and not args.no_headings:
                    util.write_csv_heading(
                        fieldmap=util.FIELDMAP_OBJECT,
                        file=args.outfile
                    )
                util.write_csv(
                    fieldmap=util.FIELDMAP_OBJECT,
                    dataObj=result,
                    file=args.outfile
                )
            else:
                print(result)
            count_output += 1
        else:
            logging.error(f"{objectID} is not a valid object id!")
except KeyboardInterrupt:
    exit()
