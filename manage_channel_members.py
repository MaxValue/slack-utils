#!/usr/bin/env python3
# coding: utf-8
#

import argparse, logging, requests, json, sys, os
import slack_utils as util

ENUM_MODE_ADD = [
    "add",
    "invite",
]

ENUM_MODE_DEL = [
    "del",
    "kick",
    "delete",
    "remove",
]

ENUM_MODE_TEST = [
    "test"
]

def invite(members, channelObj, util=util, allmembers=False):
    if type(members) not in [list, set, tuple]:
        members = [members]
    channelID = channelObj.get("id", "")
    allUsers = util.return_all(util._client.users_list, datakey="members")
    if args.all:
        logging.info(f"Everyone will be added to {channelID}")
        givenUsers = util.filterUsers(
            allUsers=allUsers,
            desiredUsers=allUsers,
            raw=True
        )
    else:
        givenUsers = util.filterUsers(
            allUsers=allUsers,
            desiredUsers=members,
            raw=True
        )
    existingMembers = util.return_all(util._client.conversations_members, datakey="members", channel=channelID)
    noninvitableMembers = util.get_userObj(existingMembers) + [util._userInfo]
    logging.debug(f"Non-invitable users: {noninvitableMembers}")
    newMembers = util.substractObjLists(givenUsers, noninvitableMembers)
    for i in range(0, len(newMembers), 1000):
        newMemberBatch = newMembers[i:i+1000]
        logging.info(f"Inviting {len(newMemberBatch)} users to {channelObj.get('name',channelID)}")
        results = util.return_one(
            util._client.conversations_invite,
            channel=channelID,
            users=util.extractKey(newMemberBatch, string=True),
        )
    return newMembers

def kick(members, channelObj, util=util, allmembers=False):
    if type(members) not in [list, set, tuple]:
        members = [members]
    channelID = channelObj.get("id", "")
    allUsers = util.return_all(util._client.users_list, datakey="members")
    if args.all:
        logging.info(f"Everyone will be removed from {channelID}")
        givenUsers = util.filterUsers(
            allUsers=allUsers,
            desiredUsers=allUsers,
            raw=True
        )
    else:
        givenUsers = util.filterUsers(
            allUsers=allUsers,
            desiredUsers=members,
            raw=True
        )
    existingMembers = util.return_all(util._client.conversations_members, datakey="members", channel=channelID)
    nonKickableMembers = [util._userInfo]
    logging.debug(f"Non-kickalbe users: {nonKickableMembers}")
    kickingMembers = util.substractObjLists(givenUsers, nonKickableMembers)
    for kickingMember in kickingMembers:
        logging.info(f"Kicking {kickingMember} from {channelObj.get('name',channelID)}")
        results = util.return_one(
            util._client.conversations_kick,
            channel=channelID,
            user=util.extractKey([kickingMember], string=True),
        )
    return kickingMembers

def test(members, channelObj, util=util, allmembers=False):
    if type(members) not in [list, set, tuple]:
        members = [members]
    channelID = channelObj.get("id", "")
    allUsers = util.return_all(util._client.users_list, datakey="members")
    if args.all:
        logging.info(f"Everyone will be tested for membership in {channelID}")
        givenUsers = util.filterUsers(
            allUsers=allUsers,
            desiredUsers=allUsers,
            raw=True
        )
    else:
        givenUsers = util.filterUsers(
            allUsers=allUsers,
            desiredUsers=members,
            raw=True
        )
    existingMembers = util.return_all(util._client.conversations_members, datakey="members", channel=channelID)
    result = []
    for member in givenUsers:
        if member["id"] in existingMembers:
            result.append(True)
        else:
            result.append(False)
    return result

if __name__ == '__main__':
    parser = argparse.ArgumentParser(argument_default=False, description="Invite/kick multiple users for a conversation (open or closed channel).")
    parser.add_argument("--all", action="store_true", help="Select all users in the workspace.")
    parser.add_argument("--verbose", "-v", action="count", default=0, help="Turn on verbose mode.")
    parser.add_argument("--quiet", "-q", action="store_true", help="Only return data but no log messages.")
    parser.add_argument("--log", help="Logfile path. If omitted, stdout is used.")
    parser.add_argument("--debug", "-d", action="store_true", help="Log all messages including debug.")
    parser.add_argument("--csv", action="store_true", help="Return data as CSV row.")
    parser.add_argument("--no-headings", action="store_true", help="Do not print CSV header.")
    parser.add_argument("--immediate", action="store_true", help="When piping users to the script, immediately invite/kick them instead of waiting for EOT.")
    parser.add_argument("--outfile", type=argparse.FileType("w", encoding="utf-8"), default=sys.stdout, help="The file in which to save the export data to.")
    parser.add_argument("--user-token", default=os.environ.get("SLACK_USER_TOKEN", False), help="The user authorization token. An environment variable (SLACK_USER_TOKEN) is recommended though.")
    parser.add_argument("--bot-token", default=os.environ.get("SLACK_BOT_TOKEN", False), help="The bot authorization token. An environment variable (SLACK_BOT_TOKEN) is recommended though.")
    parser.add_argument("channel", help="The id or object of the channel.")
    parser.add_argument("mode", choices=ENUM_MODE_ADD+ENUM_MODE_DEL+ENUM_MODE_TEST, help="Whether to add or remove users.")
    parser.add_argument("users", nargs="?", default=False, help="The user IDs or user objects to manage.")
    args = parser.parse_args()

    if not args.user_token or not args.bot_token:
        parser.error("No token provided! Provide an env var (SLACK_USER_TOKEN or SLACK_BOT_TOKEN) or use --user-token / --bot-token")

    if args.quiet:
        loglevel = 100
    elif args.debug:
        loglevel = logging.DEBUG
    elif args.verbose:
        loglevel = logging.INFO
    else:
        loglevel = logging.WARNING

    if args.log:
        logging.basicConfig(filename=args.log, filemode="a", level=loglevel)
    else:
        logging.basicConfig(level=loglevel)

    if args.mode in ENUM_MODE_ADD:
        modeMethod = invite
    elif args.mode in ENUM_MODE_DEL:
        modeMethod = kick
    elif args.mode in ENUM_MODE_TEST:
        modeMethod = test
    else:
        parser.error(f"mode parameter must be one of {(ENUM_MODE_ADD + ENUM_MODE_DEL + ENUM_MODE_TEST).join(', ')}")

    client = util.client(
        token=args.user_token,
        timeout=120,
        userinfo=True,
    )

    channelObj = util.get_channelObj(args.channel)[0]

    if args.users:
        userObjects = util.get_userObj(args.users)
        changedMembers = modeMethod(members=userObjects, channelObj=channelObj, allmembers=args.all)
    elif not sys.stdin.isatty():
        if args.immediate:
            changedMembers = []
            for line in sys.stdin:
                userObject = util.get_userObj(line)
                changedMembers += modeMethod(members=[userObject], channelObj=channelObj, allmembers=args.all)
        else:
            userObjects = util.get_userObj(sys.stdin)
            changedMembers = modeMethod(members=userObjects, channelObj=channelObj, allmembers=args.all)
    count_output = 0
    for changedMember in changedMembers:
        if args.csv:
            if count_output == 0 and not args.no_headings:
                util.write_csv_heading(
                    fieldmap=util.FIELDMAP_USER,
                    file=args.outfile
                )
            util.write_csv(
                fieldmap=util.FIELDMAP_USER,
                dataObj=changedMember,
                file=args.outfile
            )
        else:
            print(json.dumps(changedMember))
