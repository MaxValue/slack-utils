#!/usr/bin/env bash

source .venv/bin/activate
set -o allexport
source "$1"
set +o allexport
