#!/usr/bin/env python3
# coding: utf-8
#

import argparse
import logging
import os
import pandas
import slack_utils as util
import sys

parser = argparse.ArgumentParser(argument_default=False, description="Gets all members of a channel.")
parser.add_argument("channels", nargs="+", help="The identifications of the channels.")
parser.add_argument("--verbose", "-v", action="count", default=0, help="Turn on verbose mode.")
parser.add_argument("--quiet", "-q", action="store_true", help="Only return data but no log messages.")
parser.add_argument("--log", help="Logfile path. If omitted, stdout is used.")
parser.add_argument("--debug", "-d", action="store_true", help="Log all messages including debug.")
parser.add_argument("--csv", action="store_true", help="Return data as CSV row.")
parser.add_argument("--no-headings", action="store_true", help="Do not print CSV header.")
parser.add_argument("--outfile", type=argparse.FileType("w", encoding="utf-8"), default=sys.stdout, help="The file in which to save the export data to.")
parser.add_argument("--user-token", default=os.environ.get("SLACK_USER_TOKEN", False), help="The user authorization token. An environment variable (SLACK_USER_TOKEN) is recommended though.")
parser.add_argument("--bot-token", default=os.environ.get("SLACK_BOT_TOKEN", False), help="The bot authorization token. An environment variable (SLACK_BOT_TOKEN) is recommended though.")
args = parser.parse_args()

if not args.user_token or not args.bot_token:
    parser.error("No token provided! Provide an env var (SLACK_USER_TOKEN or SLACK_BOT_TOKEN) or use --user-token / --bot-token")

if args.quiet:
    loglevel = 100
elif args.debug:
    loglevel = logging.DEBUG
elif args.verbose:
    loglevel = logging.INFO
else:
    loglevel = logging.WARNING

if args.log:
    logging.basicConfig(filename=args.log, filemode="a", level=loglevel)
else:
    logging.basicConfig(level=loglevel)
logger = logging.getLogger(__name__)

client = util.client(token=args.user_token)

channelObjs = util.get_channelObj(args.channels)

count_output = 0

for channelObj in channelObjs:
    channelID = channelObj["id"]
    if util.re_channelID.match(channelID):
        found_users = pandas.DataFrame(
            data = util.return_all(client.conversations_members, datakey="members", channel=channelID),
            columns = ["id"],
        )
    else:
        parser.error(f"{channelID} is not a valid channel id!")

    logger.info(f"Found {len(found_users)} matching users.")

    if args.csv:
        found_users.to_csv(
            path_or_buf=args.outfile,
            sep=";",
            header=(False if args.no_headings else True),
            index=False,
            # index_label=
            mode="a",
            encoding="utf-8",
            line_terminator="\n",
        )
    else:
        found_users.to_json(
            path_or_buf=args.outfile,
            index=False,
        )
