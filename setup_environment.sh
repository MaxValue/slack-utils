#!/usr/bin/env bash

python3 -m venv --clear .venv
source .venv/bin/activate
	pip3 install --upgrade pip
	pip3 install --upgrade wheel
	pip3 install --upgrade --requirement requirements.txt
deactivate
