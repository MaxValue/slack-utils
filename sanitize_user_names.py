#!/usr/bin/env python3
# coding: utf-8
#
# print only
# perform sync, print unknown
# confirm each

import argparse, logging, os, sys, json, re
import slack_utils as util

re_varname = re.compile(r"^(?P<groupname>regex)$", re.IGNORECASE)
re_user_name_fridaysforfutureat = re.compile(r"^(?P<firstname>[A-ZÄËÏÖÜ][a-zäëïöü]+)( (?P<middlenames>[A-ZÄËÏÖÜa-zäëïöü ]+))?? (?P<lastname>([A-ZÄËÏÖÜ]\.|[A-ZÄËÏÖÜ][a-zäëïöü]+)) \([A-ZÄËÏÖÜ][-\./ A-Za-zäëïöü]+\)$")
re_USERNAME = re_user_name_fridaysforfutureat

parser = argparse.ArgumentParser(argument_default=False, description="Sanitizes the names of users of a workspace.")
parser.add_argument("--sync", action="store_true", help="Synchronize display name and real name.")
parser.add_argument("--step", action="store_true", help="Ask for confirmation on each change.")
parser.add_argument("--verbose", "-v", action="count", default=0, help="Turn on verbose mode.")
parser.add_argument("--quiet", "-q", action="store_true", help="Only return data but no log messages.")
parser.add_argument("--log", help="Logfile path. If omitted, stdout is used.")
parser.add_argument("--debug", "-d", action="store_true", help="Log all messages including debug.")
parser.add_argument("--csv", action="store_true", help="Return data as CSV row.")
parser.add_argument("--no-headings", action="store_true", help="Do not print CSV header.")
parser.add_argument("--outfile", type=argparse.FileType("w", encoding="utf-8"), default=sys.stdout, help="The file in which to save the export data to.")
parser.add_argument("--user-token", default=os.environ.get("SLACK_USER_TOKEN", False), help="The user authorization token. An environment variable (SLACK_USER_TOKEN) is recommended though.")
parser.add_argument("--bot-token", default=os.environ.get("SLACK_BOT_TOKEN", False), help="The bot authorization token. An environment variable (SLACK_BOT_TOKEN) is recommended though.")
args = parser.parse_args()

if not args.user_token or not args.bot_token:
    parser.error("No token provided! Provide an env var (SLACK_USER_TOKEN or SLACK_BOT_TOKEN) or use --user-token / --bot-token")

if args.quiet:
    loglevel = 100
elif args.debug:
    loglevel = logging.DEBUG
elif args.verbose:
    loglevel = logging.INFO
else:
    loglevel = logging.WARNING

if args.log:
    logging.basicConfig(filename=args.log, filemode="a", level=loglevel)
else:
    logging.basicConfig(level=loglevel)

client = util.client(token=args.user_token)

users_to_check = []
users_to_sync = []
users_to_sync_decide = []
count = 0

logging.debug("Getting all users.")
all_users = util.return_all(client.users_list, datakey="members")
logging.debug(f"Finding user in user list.")
for user in all_users:
    if user["is_bot"] or user["deleted"]:
        continue
    user_realName = user.get("profile", {}).get("real_name", "")
    user_displayName = user.get("profile", {}).get("display_name", "")
    user_realName_match = re_USERNAME.match(user_realName)
    user_displayName_match = re_USERNAME.match(user_displayName)
    if user_realName == user_displayName:
        if not user_realName_match:
            logging.debug(f"{user['id']} Both names are incorrect: real: {user_realName!r} display: {user_displayName!r}")
            users_to_check.append(user)
    else:
        if user_realName_match and user_displayName_match:
            logging.debug(f"{user['id']} Both names ok, but differ: real: {user_realName!r} display: {user_displayName!r}")
            users_to_sync_decide.append(user)
        elif user_realName_match:
            if user_displayName != "":
                logging.debug(f"{user['id']} Only real name ok: real: {user_realName!r} display: {user_displayName!r}")
                finalUser = dict(user)
                finalUser["correct_name_field"] = "real_name"
                finalUser["target_name_field"] = "display_name"
                users_to_sync.append(finalUser)
        elif user_displayName_match:
            if user_realName != "":
                logging.debug(f"{user['id']} Only display name ok: real: {user_realName!r} display: {user_displayName!r}")
                finalUser = dict(user)
                finalUser["correct_name_field"] = "display_name"
                finalUser["target_name_field"] = "real_name"
                users_to_sync.append(finalUser)
        else:
            logging.debug(f"{user['id']} Both names not ok and different: {user_realName!r} display: {user_displayName!r}")
            users_to_check.append(user)

count_output = 0

if len(users_to_sync):
    logging.info(f"Found {len(users_to_sync)} users to synchronize.")
    for user in users_to_sync:
        if args.sync:
            if args.step:
                choice = ""
                while choice.lower() not in ["y", "n"]:
                    choice = input(f"User {user['id']}: Change {user['profile'][user['target_name_field']]!r} to {user['profile'][user['correct_name_field']]!r} [y/N]:")
            else:
                choice = "y"
            if choice == "y":
                util.return_one(
                    client.users_profile_set,
                    user=user['id'],
                    name=user["target_name_field"],
                    value=user["profile"][user["correct_name_field"]]
                )
        else:
            if args.csv:
                if count_output == 0 and not args.no_headings:
                    util.write_csv_heading(
                        fieldmap=util.FIELDMAP_USER,
                        file=args.outfile
                    )
                util.write_csv(
                    fieldmap=util.FIELDMAP_USER,
                    dataObj=user,
                    file=args.outfile
                )
            else:
                print(json.dumps(user, separators=(',', ':')), file=args.outfile)
        count_output += 1

decision_switch = [
    "real_name",
    "display_name",
]

if len(users_to_sync_decide):
    logging.info(f"Found {len(users_to_sync_decide)} users to manually decide for syncing.")
    for user in users_to_sync_decide:
        if args.sync:
            choice = ""
            while choice.lower() not in ["1", "2", "3"]:
                print(f"User {user['id']}, keep either:")
                print(f"\t1. real name: {util.jmes('profile.real_name', user)}")
                print(f"\t2. display name: {util.jmes('profile.display_name', user)}")
                print(f"\t3. Skip")
                choice = input(f"Keep [1/2/3]:")
            if choice in ["1","2"]:
                util.return_one(
                    client.users_profile_set,
                    user=user['id'],
                    name=decision_switch[int(choice)%len(decision_switch)],
                    value=user["profile"][decision_switch[int(choice)-1]]
                )
        else:
            if args.csv:
                if count_output == 0 and not args.no_headings:
                    util.write_csv_heading(
                        fieldmap=util.FIELDMAP_USER,
                        file=args.outfile
                    )
                util.write_csv(
                    fieldmap=util.FIELDMAP_USER,
                    dataObj=user,
                    file=args.outfile
                )
            else:
                print(json.dumps(user, separators=(',', ':')), file=args.outfile)
        count_output += 1

if len(users_to_check):
    logging.info(f"Found {len(users_to_check)} users which need manual checking, because no names are correct.")
    for user in users_to_check:
        if args.csv:
            if count_output == 0 and not args.no_headings:
                util.write_csv_heading(
                    fieldmap=util.FIELDMAP_USER,
                    file=args.outfile
                )
            util.write_csv(
                fieldmap=util.FIELDMAP_USER,
                dataObj=user,
                file=args.outfile
            )
        else:
            print(json.dumps(user, separators=(',', ':')), file=args.outfile)
        count_output += 1
