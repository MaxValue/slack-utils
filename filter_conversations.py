#!/usr/bin/env python3
# coding: utf-8
#

import argparse
import json
import logging
import os
import pandas
import slack_utils as util
import sys

logger = logging.getLogger(__name__)

parser = argparse.ArgumentParser(argument_default=False, description="Finds conversations inside of a workspace.")
# parser.add_argument("--last-active", type=int, help="Filter for conversations last active before this time (in EPOCH).")
parser.add_argument("--name", help="Filter for conversations with their names containing this string.")
parser.add_argument("--type", nargs="*", default=["all"], choices=["all","open","closed","mpim","im","group","channel"], help="Filter for conversations with their names containing this string.\nA group is a 'closed' channel, a channel is an 'open' channel.")
parser.add_argument("--include-archived", action="store_true", help="Also search through archived conversations.")
parser.add_argument("--count", "-c", type=int, help="Only return matches if this number of conversations is found.")
parser.add_argument("--exact", action="store_true", help="Only exact matches. Does not apply to --id.")
parser.add_argument("--id", help="Filter for conversations which have this Slack id.")
parser.add_argument("--verbose", "-v", action="count", default=0, help="Turn on verbose mode.")
parser.add_argument("--quiet", "-q", action="store_true", help="Only return data but no log messages.")
parser.add_argument("--log", help="Logfile path. If omitted, stdout is used.")
parser.add_argument("--debug", "-d", action="store_true", help="Log all messages including debug.")
parser.add_argument("--csv", action="store_true", help="Return data as CSV row.")
parser.add_argument("--no-headings", action="store_true", help="Do not print CSV header.")
parser.add_argument("--outfile", type=argparse.FileType("w", encoding="utf-8"), default=sys.stdout, help="The file in which to save the export data to.")
parser.add_argument("--user-token", default=os.environ.get("SLACK_USER_TOKEN", False), help="The user authorization token. An environment variable (SLACK_USER_TOKEN) is recommended though.")
parser.add_argument("--bot-token", default=os.environ.get("SLACK_BOT_TOKEN", False), help="The bot authorization token. An environment variable (SLACK_BOT_TOKEN) is recommended though.")
args = parser.parse_args()

if not args.user_token or not args.bot_token:
    parser.error("No token provided! Provide an env var (SLACK_USER_TOKEN or SLACK_BOT_TOKEN) or use --user-token / --bot-token")

args.type = set(args.type)
if "open" in args.type:
    args.type.remove("open")
    args.type.add("public_channel")
if "channel" in args.type:
    args.type.remove("channel")
    args.type.add("public_channel")
if "closed" in args.type:
    args.type.remove("closed")
    args.type.add("private_channel")
if "group" in args.type:
    args.type.remove("group")
    args.type.add("private_channel")
if "all" in args.type:
    args.type.update(set(["mpim","im","private_channel","public_channel"]))
    args.type.remove("all")

if args.quiet:
    loglevel = 100
elif args.debug:
    loglevel = logging.DEBUG
elif args.verbose:
    loglevel = logging.INFO
else:
    loglevel = logging.WARNING

if args.log:
    logging.basicConfig(filename=args.log, filemode="a", level=loglevel)
else:
    logging.basicConfig(level=loglevel)

logger.debug(f"Requested conversation types: {args.type!r}")

client = util.client(token=args.user_token)

found_conversations = []
count = 0

kwargs = {}
if args.include_archived:
    kwargs["exclude_archived"] = False
else:
    kwargs["exclude_archived"] = True

logger.debug("Getting all conversations.")
all_conversations = pandas.json_normalize(
    data = util.return_all(
                client.conversations_list,
                datakey="channels",
                limit=1000,
                types=",".join(args.type),
                **kwargs
    ),
)

query = ""
if args.id:
    query += f"`id`.lower() == {args.id.lower()!r}"
if args.name:
    query += f"{' and ' if len(query) else ''}`name`.lower() {'==' if args.exact else 'in'} {args.name.lower()!r} "
logger.debug(f"Finding conversation in conversation list.")
if query.strip() != "":
    all_conversations.query(
        expr = query,
        inplace = True,
    )

returnResults = False
if args.count:
    if len(all_conversations.index) == args.count:
        returnResults = True
        logger.info(f"Found {len(all_conversations.index)} matching conversations (exactly {args.count} were found).")
    else:
        returnResults = False
        logger.debug(f"Not returning any matches because found number of conversations does not equal given conversation count.")
else:
    returnResults = True
    logger.info(f"Found {len(all_conversations.index)} matching conversations.")

if returnResults:
    if args.csv:
        all_conversations.to_csv(
            path_or_buf=args.outfile,
            sep=";",
            header=(False if args.no_headings else True),
            index=False,
            # index_label=
            mode="a",
            encoding="utf-8",
            lineterminator="\n",
        )
    else:
        all_conversations.to_json(
            path_or_buf=args.outfile,
            index=False,
        )
