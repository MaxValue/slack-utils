#!/usr/bin/env python3
# coding: utf-8

import slack

class MissingScopeException(slack.errors.SlackApiError):
    def __init__(self, response, scopes, message="Missing Scopes: {scopes}"):
        self.scopes = scopes
        self.message = message.format(scopes=", ".join(self.scopes))
        super().__init__(message=self.message, response=response)

class FetchMembersFailedException(slack.errors.SlackApiError):
    def __init__(self, response, url, message="Failed to fetch users with request {url}."):
        self.url = url
        self.message = message.format(url=self.url)
        super().__init__(message=self.message, response=response)

class CantKickSelfException(slack.errors.SlackApiError):
    def __init__(self, response, url, message="You cannot kick yourself ({url})."):
        self.url = url
        self.message = message.format(url=self.url)
        super().__init__(message=self.message, response=response)

class NotInChannelException(slack.errors.SlackApiError):
    def __init__(self, response, url, message="The user is not in the channel ({url})."):
        self.url = url
        self.message = message.format(url=self.url)
        super().__init__(message=self.message, response=response)

class UserNotFoundException(slack.errors.SlackApiError):
    def __init__(self, response, url, message="User not found ({url})."):
        self.url = url
        self.message = message.format(url=self.url)
        super().__init__(message=self.message, response=response)

class RatelimitedException(slack.errors.SlackApiError):
    def __init__(self, response, url, message="Ratelimited ({url})."):
        self.url = url
        self.message = message.format(url=self.url)
        super().__init__(message=self.message, response=response)

class OverPaginationLimitException(slack.errors.SlackApiError):
    def __init__(self, response, url, message="Over pagination limit ({url})."):
        self.url = url
        self.message = message.format(url=self.url)
        super().__init__(message=self.message, response=response)

class CantInviteException(slack.errors.SlackApiError):
    def __init__(self, response, url, message="Cannot invite user ({url})."):
        self.url = url
        self.message = message.format(url=self.url)
        super().__init__(message=self.message, response=response)

class CantInviteSelfException(slack.errors.SlackApiError):
    def __init__(self, response, url, message="You cannot invite yourself ({url})."):
        self.url = url
        self.message = message.format(url=self.url)
        super().__init__(message=self.message, response=response)

class CantInviteAppException(slack.errors.SlackApiError):
    def __init__(self, response, url, message="Cannot invite app user ({url})."):
        self.url = url
        self.message = message.format(url=self.url)
        super().__init__(message=self.message, response=response)

class UnknownException(slack.errors.SlackClientError):
    def __init__(self, response, url, message="Unknown exception ({url})\n{response}"):
        self.url = url
        self.message = message.format(url=self.url, response=self.response)
        super().__init__(message=self.message)
