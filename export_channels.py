#!/usr/bin/env python3
# coding: utf-8

import click
import click_aliases
import datetime
import json
import logging
import os
import shutil
import slack_utils as util

logger = logging.getLogger(__name__)

# FIXME test with inexistent channel

CHANNELINFO_ALLOWEDKEYS = [
    'id',
    'name',
    'created',
    'creator',
    'is_archived',
    'is_general',
    'members',
    'topic',
    'purpose',
]

def createexportDir(outfolder, nametmpl="{teamSlug} Slack channels {time:%Y-%m-%dT%H%M}", **context):
    exportDir = os.path.join(outfolder, nametmpl.format(**context))
    os.makedirs(exportDir, exist_ok=True)
    return exportDir


@click.group()
@click.option("--debug/--no-debug", default=False, envvar="DEBUG", help="Log all messages including debug.")
@click.option("--verbose/--quiet", default=False, help="Increase log messages or turn them off entirely.")
@click.option("--user-token", envvar="SLACK_USER_TOKEN", required=True, help="The user authorization token. An environment variable (SLACK_USER_TOKEN) is recommended though.")
@click.option("--bot-token", envvar="SLACK_BOT_TOKEN", help="The bot authorization token. An environment variable (SLACK_BOT_TOKEN) is recommended though.")
@click.option("--log", type=click.Path(dir_okay=False,writable=True), help="Logfile path. If omitted, stdout is used.")
@click.pass_context
def cli(ctx, debug, verbose, user_token, bot_token, log):
    """Exports all or specific channels of a workspace"""
    logging.basicConfig(level=logging.INFO)
    if not verbose:
        loglevel = 100
    elif debug:
        loglevel = logging.DEBUG
    elif verbose:
        loglevel = logging.INFO
    else:
        loglevel = logging.WARNING
    if log:
        logging.basicConfig(filename=log, filemode="a", level=loglevel)
    else:
        logging.basicConfig(level=loglevel)
    logger.info("Initializing API client")
    ctx.obj["client"] = util.client(token=user_token)

@cli.command(name="export")
@click.option("--channels", multiple=True, help="The channels to export. By default all channels are exported.")
@click.option("--outfolder", type=click.Path(file_okay=False,dir_okay=True,writable=True), default=os.getcwd(), help="The directory in which to save the export data to.")
@click.option("--archive/--no-archive", default=True, help="Whether to zip the export or not. The official Slack importer wants a ZIP archive after all.")
@click.pass_context
def exportChannels(ctx, channels, outfolder, archive):
    """Export the given channels."""
    logger.info("Create export dir")
    exportDir = createexportDir(
        outfolder = outfolder,
        teamSlug = util._teamInfo.get("domain", "UNKNOWN_TEAM"),
        time = datetime.datetime.now(),
    )

    logger.info("Get all conversations")
    all_conversations = util.return_all(
        endpoint=ctx.obj["client"].conversations_list,
        datakey="channels",
        limit=1000,
        types="public_channel,private_channel",
        exclude_archived=False,
    )
    channelInfos = []
    for channelBatch in all_conversations:
        for channel in channelBatch:
            if channel["name"] in channels:
                logger.info(f'Exporting conversation {channel["name"]}')
                os.makedirs(os.path.join(exportDir, channel["name"]), exist_ok=True)
                channel_history = util.return_all(
                    endpoint = ctx.obj["client"].conversations_history,
                    datakey = "messages",
                    channel = channel["id"],
                    limit = 50,
                    include_all_metadata = True,
                )
                for batchIndex, messageBatch in enumerate(channel_history):
                    finalMessageBatch = messageBatch[:]
                    for message in messageBatch:
                        if message.get("reply_count", 0) > 0:
                            thread_history = list(util.return_all(
                                endpoint = ctx.obj["client"].conversations_replies,
                                datakey = "messages",
                                channel = channel["id"],
                                ts = message["ts"],
                                limit = 50,
                                include_all_metadata = True,
                            ))
                            finalMessageBatch += thread_history
                    with open(os.path.join(exportDir, channel["name"], f'{channel["name"]}_{batchIndex:04d}.json'), "w", encoding="utf-8") as messageBatchFile:
                        json.dump(
                            obj = finalMessageBatch,
                            fp = messageBatchFile,
                            separators = (',', ':'),
                            ensure_ascii = False,
                        )
                logger.info("Rendering channel infos")
                channelInfo = dict(channel)
                for key in channel.keys():
                    if key not in CHANNELINFO_ALLOWEDKEYS:
                        del channelInfo[key]
                logger.info("Adding channel members")
                channelMembers = util.return_all(
                    endpoint=ctx.obj["client"].conversations_members,
                    datakey="members",
                    channel=channel["id"],
                    limit=100,
                )
                channelInfo["members"] = sum(channelMembers, [])
                channelInfos.append(channelInfo)
    logger.info("Storing channel infos")
    with open(os.path.join(exportDir, "channels.json"), "w", encoding="utf-8") as channelInfosFile:
        json.dump(
            obj = channelInfos,
            fp = channelInfosFile,
            separators = (',', ':'),
            ensure_ascii = False,
        )
    logger.info("Storing member infos")
    userInfos = []
    allUsers = util.return_all(
        endpoint=ctx.obj["client"].users_list,
        datakey="members",
        limit=100,
        include_locale = True,
    )
    for userBatch in allUsers:
        for user in userBatch:
            userInfos.append(
                user,
            )
    with open(os.path.join(exportDir, "users.json"), "w", encoding="utf-8") as userInfosFile:
        json.dump(
            obj = userInfos,
            fp = userInfosFile,
            separators = (',', ':'),
            ensure_ascii = False,
        )
    if archive:
        logger.info("Creating archive")
        shutil.make_archive(
            base_name = exportDir,
            format = "zip",
            root_dir = exportDir,
        )

if __name__ == "__main__":
    cli(obj={})
