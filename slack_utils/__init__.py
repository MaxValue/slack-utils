#!/usr/bin/env python3
# coding: utf-8

from collections.abc import Iterable
from http.client import RemoteDisconnected as http_client_RemoteDisconnectedException
from slack_exceptions import *
import atexit
# import csv
import datetime
import jmespath
import json
import logging
import os
import re
import slack
import sys
import time

_client = None
_teamInfo = None
_userInfo = None
_membersInfo = None

re_userID = re.compile(r"^(?P<id>(U|W)[A-Z0-9]+)$", re.IGNORECASE)
re_channelID = re.compile(r"^(?P<id>[CGD][A-Z0-9]{8,})$", re.IGNORECASE)

FIELDMAP_USER = {
    "team_id": "team_id",
    "id": "id",
    "first_name": "profile.first_name",
    "last_name": "profile.last_name",
    "real_name": ["profile.real_name", "real_name"],
    "real_name_normalized": "profile.real_name_normalized",
    "name": "name",
    "deleted": "deleted",
    "first_login": "first_login",
    "last_login": "last_login",
    "inactive_days": "inactive_days",
    "is_admin": "is_admin",
    "is_owner": "is_owner",
    "is_primary_owner": "is_primary_owner",
    "is_restricted": "is_restricted",
    "is_ultra_restricted": "is_ultra_restricted",
    "is_bot": "is_bot",
    "is_app_user": "is_app_user",
    "updated": "updated",
    "Account type": "'Account type'",
    "Account created": "'Account created'",
    "Included in bill": "'Included in bill'", # Members are only included in your bill if they’ve been active in Slack.
    "Days active": "'Days active'", # A member is active if they’ve posted a message or read at least one channel or direct message.
    "Messages posted": "'Messages posted'",
    "title": "profile.title",
    "phone": "profile.phone",
    "skype": "profile.skype",
    "Geburtsdatum": "profile.fields.XfL6JTQ3J7.value",
    "Twitter": "profile.fields.XfLJ0S34N4.value",
    "Skype": "profile.fields.XfFKT2V737.value",
    "Facebook": "profile.fields.XfLHGG6DQU.value",
    "GitHub": "profile.fields.XfLJ0UPNAU.value",
    "GitLab": "profile.fields.XfL6JNPLG2.value",
    "Instagram": "profile.fields.XfLBLP9RFT.value",
    "Anfangsdatum": "profile.fields.XfPF0K2JCB.value",
    "Website": "profile.fields.XfL6JV3GSX.value",
    "Flickr": "profile.fields.XfP1GL72GN.value",
    "YouTube": "profile.fields.XfP169NTT9.value",
    "status_text": "profile.status_text",
    "status_emoji": "profile.status_emoji",
    "status_expiration": "profile.status_expiration",
    "tz": "tz",
    "tz_label": "tz_label",
    "tz_offset": "tz_offset",
    "image_original": "profile.image_original",
    "is_custom_image": "profile.is_custom_image",
    "avatar_hash": "profile.avatar_hash",
    "image_24": "profile.image_24",
    "image_32": "profile.image_32",
    "image_48": "profile.image_48",
    "image_72": "profile.image_72",
    "image_192": "profile.image_192",
    "image_512": "profile.image_512",
    "image_1024": "profile.image_1024",
    "color": "color",
    "status_text_canonical": "profile.status_text_canonical",
}

FIELDMAP_ACCESSLOGS = {
    "user_id": "user_id",
    "username": "username",
    "date_first": "date_first",
    "date_last": "date_last",
    "count": "count",
    "ip": "ip",
    "user_agent": "user_agent",
    "isp": "isp",
    "country": "country",
    "region": "region",
}

FIELDMAP_CHANNEL = {
    "id": "id",
    "name": "name",
    "name_normalized": "name_normalized",
    "created": "created",
    "Is public channel": "is_channel",
    "Is private channel": "is_group",
    "Is chat (1on1)": "is_im",
    "Is group chat (multiple)": "is_mpim",
    "is_archived": "is_archived",
    "is_general": "is_general",
    "unlinked": "unlinked",
    "is_shared": "is_shared",
    "parent_conversation": "parent_conversation",
    "creator": "creator",
    "is_ext_shared": "is_ext_shared",
    "is_org_shared": "is_org_shared",
    "shared_team_ids": "shared_team_ids", # CHECK
    "pending_shared": "pending_shared", # CHECK
    "pending_connected_team_ids": "pending_connected_team_ids", # CHECK
    "is_pending_ext_shared": "is_pending_ext_shared",
    "is_member": "is_member",
    "is_private": "is_private",
    "topic": "topic.value",
    "purpose": "purpose.value",
    "num_members": "num_members",
}

FIELDMAP_TEAM = {
    "id": "id",
    "domain": "teamDomain",
    "name": "teamName",
    "isPlusTeam": "isPlusTeam",
    "twoFactorRequired": "twoFactorRequired",
    "twoFactorType": "twoFactorType",
    "email_domain": "email_domain",
}

FIELDMAP_REMINDER = {
    "id": "id",
    "creator": "creator",
    "target": "user",
    "text": "text",
    "recurring": "recurring",
    "time": "time",
    "completed at": "complete_ts",
}

def client(teaminfo=True, userinfo=False, *args, **kwargs):
    global _client, _teamInfo, _userInfo
    _client = slack.WebClient(*args, **kwargs)
    if teaminfo:
        _teamInfo = return_one(_client.team_info, key="team")
    if userinfo:
        _userInfo = return_one(_client.users_identity, key="user")
    return _client

def return_one(endpoint, key=False, retries=3, **kwargs):
    logging.debug(f"Preparing for {endpoint}")
    params = {"cursor": ""}
    params.update(kwargs)
    for retry in range(retries):
        logging.debug(f"Try {retry+1}")
        try:
            response = endpoint(**kwargs)

            break
        except slack.errors.SlackApiError as e:
            response = e.response
            errorcode = response["error"]
            if errorcode == "missing_scope":
                raise MissingScopeException(scopes=response["needed"].split(","), response=response) from e
            elif errorcode == "fetch_members_failed":
                raise FetchMembersFailedException(url=response.url, response=response) from e
            elif errorcode == "cant_kick_self":
                raise CantKickSelfException(url=response.url, response=response) from e
            elif errorcode == "not_in_channel":
                raise NotInChannelException(url=response.url, response=response) from e
            elif errorcode == "user_not_found":
                raise UserNotFoundException(url=response.url, response=response) from e
            elif errorcode == "ratelimited":
                # raise RatelimitedException(url=response.url, wait=int(response.headers.get("retry-after", 0)), response=response) from e
                delay = int(response.headers['Retry-After'])
                logging.info(f"Ratelimited, waiting {delay} seconds.")
                time.sleep(delay)
            elif errorcode == "cant_invite":
                raise CantInviteException(url=response.url, response=response) from e
            elif errorcode == "cant_invite_self":
                raise CantInviteSelfException(url=response.url, response=response) from e
            elif errorcode == "cant_invite_app_user":
                raise CantInviteAppException(url=response.url, response=response) from e
            elif errorcode == "over_pagination_limit":
                raise OverPaginationLimitException(url=response.url, response=response) from e
            else:
                raise e
        except http_client_RemoteDisconnectedException as e:
            pass
    else:
        raise UnknownException(url=response.url, response=response) from e
    payload = response.data
    logging.debug(f"No error occured, adding to result")
    if key:
        return payload[key]
    else:
        return payload

def return_all(endpoint, datakey, pagingkey=None, **kwargs):
    logging.debug(f"Preparing for {endpoint}")
    reachedEnd = False
    next_cursor = ""
    while not reachedEnd:
        payload = return_one(endpoint, **kwargs)
        result = jmespath.search(datakey, payload)
        if not pagingkey:
            pagingkey = "paging"
        next_cursor = jmespath.search("response_metadata.next_cursor", payload)
        current_page = jmespath.search(f"{pagingkey}.page", payload)
        if not current_page:
            current_page = 1
        max_page = jmespath.search(f"{pagingkey}.pages", payload)
        if not max_page:
            max_page = 1
        next_page = False
        if current_page < 100 and current_page < max_page:
            next_page = current_page + 1
        if next_cursor:
            logging.debug(f"Next cursor is {next_cursor}")
            kwargs["cursor"] = next_cursor
        elif next_page:
            logging.debug(f"Next page is {next_page}")
            kwargs["page"] = next_page
        else:
            logging.debug(f"Reached end!")
            reachedEnd = True
        yield result

def write_csv_heading(fieldmap, file=sys.stdout):
    fieldnames = fieldmap.keys()
    csv_writer = csv.DictWriter(file, fieldnames=fieldnames, delimiter=";", lineterminator="\n")
    csv_writer.writeheader()

def write_csv(fieldmap, dataObj, file=sys.stdout):
    fieldnames = fieldmap.keys()
    result = flatten_dict(fieldmap, dataObj)
    csv_writer = csv.DictWriter(file, fieldnames=fieldnames, delimiter=";", lineterminator="\n")
    csv_writer.writerow(result)

def flatten_dict(fieldmap, dataObj, restval=None):
    result = {}
    for fieldname, fieldaddress in fieldmap.items():
        subObj = dict(dataObj)
        fieldvalue = None
        if type(fieldaddress) == list:
            for fieldaddress_alt in fieldaddress:
                fieldvalue = jmespath.search(fieldaddress_alt, subObj)
                if type(fieldvalue) != None:
                    break
        else:
            fieldvalue = jmespath.search(fieldaddress, subObj)
        if type(fieldvalue) == None:
            fieldvalue = restval
        result[fieldname] = fieldvalue
    return result

def get_userObj(inputUsers, skip_errors=True):
    if not issubclass(type(inputUsers), Iterable) or issubclass(type(inputUsers), str) or issubclass(type(inputUsers), dict):
        inputUsers = [inputUsers]
    result = []
    for inputUser in inputUsers:
        if type(inputUser) in (bytes, str):
            try:
                userObj = json.loads(fr"{inputUser}")
                if type(userObj) == str:
                    userID = userObj
                    userObj = {
                        "id": userID
                    }
                else:
                    userID = userObj["id"]
            except (json.decoder.JSONDecodeError, TypeError):
                userID = inputUser.strip()
                userObj = {
                    "id": userID
                }
                logging.debug(userObj)
        elif type(inputUser) == dict:
            userObj = inputUser
            userID = userObj["id"]
        else:
            if skip_errors:
                continue
            else:
                raise TypeError("Given users are not of type str, bytes or dict")
        if not re_userID.match(userID):
            logging.error(f"{userID} is not a valid user id!")
            if skip_errors:
                continue
        result.append(userObj)
    logging.debug(f"Result is {result}")
    return result

def get_channelObj(inputChannels, skip_errors=True):
    if not issubclass(type(inputChannels), Iterable) or issubclass(type(inputChannels), str) or issubclass(type(inputChannels), dict):
        inputChannels = [inputChannels]
    result = []
    for inputChannel in inputChannels:
        if type(inputChannel) in (bytes, str):
            try:
                channelObj = json.loads(fr"{inputChannel}")
                if type(channelObj) == str:
                    channelID = channelObj
                    channelObj = {
                        "id": channelID
                    }
                else:
                    channelID = channelObj["id"]
            except (json.decoder.JSONDecodeError, TypeError):
                channelID = inputChannel.strip()
                if channelID.startswith("#"):
                    all_conversations = return_all(
                                            _client.conversations_list,
                                            datakey="channels",
                                            limit=1000,
                                            types="public_channel,private_channel",
                                            exclude_archived=True,
                                        )
                    for conversation in all_conversations:
                        if conversation.get("name", "") == channelID[1:]:
                            channelObj = conversation
                            channelID = channelObj.get("id", "")
                            break
                    else:
                        logging.error(f"Cannot find Channel {channelID!r}!")
                        if skip_errors:
                            continue
                else:
                    channelObj = {
                        "id": channelID
                    }
        elif type(inputChannel) == dict:
            channelObj = inputChannel
            channelID = channelObj["id"]
        else:
            if skip_errors:
                continue
            else:
                raise TypeError("Given channels are not of type str, bytes or dict")
        if not re_channelID.match(channelID):
            logging.error(f"{channelID} is not a valid channel id!")
            if skip_errors:
                continue
        result.append(channelObj)
    logging.debug(f"Result is {result}")
    return result

def filterUsers(allUsers, desiredUsers, raw=False, restricted=False, ultra_restricted=False, app_user=False, bot=False, slackbot=False):
    result = []
    try:
        if raw:
            desiredUsers = get_userObj(desiredUsers)
        for desiredUser in desiredUsers:
            for user in allUsers:
                if not (user.get("id", "") == desiredUser.get("id", "") and user.get("id", False)):
                    continue
                if not ultra_restricted and user.get("is_ultra_restricted", False):
                    continue
                if not restricted and user.get("is_restricted", False):
                    continue
                if not app_user and user.get("is_app_user", False):
                    continue
                if not bot and user.get("is_bot", False):
                    continue
                if not slackbot and user.get("id", "") == "USLACKBOT":
                    continue
                result.append(user)
        return result
    except KeyboardInterrupt:
        exit()

def substractObjLists(listA, listB, key="id"):
    result = []
    for itemA in listA:
        for itemB in listB:
            if itemA.get(key, "") == itemB.get(key, "") and itemA.get(key, False):
                break
        else:
            result.append(itemA)
    return result

def extractKey(iterable, key="id", string=False, delimiter=",", default=None, emptyval=[None, "", [], (), set()]):
    result = []
    for item in iterable:
        jmesResult = jmespath.search(key, item)
        if jmesResult in emptyval and default != None:
            result.append(default)
        else:
            result.append(jmesResult)
    if string:
        return delimiter.join(result)
    else:
        return result

def jmes(path, dataObj, default=None, emptyval=[None, "", [], (), set()]):
    return extractKey(
        iterable=[dataObj],
        key=path,
        string=False,
        delimiter="",
        default=default,
        emptyval=emptyval,
    )[0]

