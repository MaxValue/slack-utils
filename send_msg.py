#!/usr/bin/env python3
# coding: utf-8

import argparse
import json
import logging
import os
import requests
import slack_utils as util
import sys

logger = logging.getLogger(__name__)

def sendMsg(members, msgObj, util=util, allmembers=False):
    logger.debug(f"members is {members}")
    logger.debug(f"msgObj is {msgObj}")
    logger.debug(f"allmembers is {allmembers}")
    result = []
    if type(members) not in [list, set, tuple]:
        members = [members]
    allUsers = util.return_all(util._client.users_list, datakey="members")
    if args.all:
        logger.info(f"Everyone will receive the message")
        givenUsers = util.filterUsers(
            allUsers=allUsers,
            desiredUsers=allUsers,
            raw=True
        )
    else:
        givenUsers = util.filterUsers(
            allUsers=allUsers,
            desiredUsers=members,
            raw=True
        )
    for member in givenUsers:
        response = util.return_one(
            util._client.chat_postMessage,
            channel=util.extractKey([member])[0],
            blocks=util.jmes("blocks", msgObj),
            text=util.jmes("text", msgObj),
        )
        if util.jmes("ok", response):
            result.append(member)
    return result

if __name__ == '__main__':
    parser = argparse.ArgumentParser(argument_default=False, description="Send a message to multiple users.")
    parser.add_argument("--all", action="store_true", help="Select all users in the workspace.")
    parser.add_argument("--verbose", "-v", action="count", default=0, help="Turn on verbose mode.")
    parser.add_argument("--quiet", "-q", action="store_true", help="Only return data but no log messages.")
    parser.add_argument("--log", help="Logfile path. If omitted, stdout is used.")
    parser.add_argument("--debug", "-d", action="store_true", help="Log all messages including debug.")
    parser.add_argument("--csv", action="store_true", help="Return data as CSV row.")
    parser.add_argument("--no-headings", action="store_true", help="Do not print CSV header.")
    parser.add_argument("--immediate", action="store_true", help="When piping users to the script, immediately send the message instead of waiting for EOT.")
    parser.add_argument("--outfile", type=argparse.FileType("w", encoding="utf-8"), default=sys.stdout, help="The file in which to save the export data to.")
    parser.add_argument("--user-token", default=os.environ.get("SLACK_USER_TOKEN", False), help="The user authorization token. An environment variable (SLACK_USER_TOKEN) is recommended though.")
    parser.add_argument("--bot-token", default=os.environ.get("SLACK_BOT_TOKEN", False), help="The bot authorization token. An environment variable (SLACK_BOT_TOKEN) is recommended though.")
    parser.add_argument("msg", type=argparse.FileType("r", encoding="utf-8"), help="The filepath to the message content.")
    parser.add_argument("users", nargs="?", default=False, help="The user IDs or user objects to manage.")
    # parser.add_argument("users", type=argparse.FileType("r", encoding="utf-8"), help="The json list of the users as file.")

    args = parser.parse_args()

    if not args.user_token or not args.bot_token:
        parser.error("No token provided! Provide an env var (SLACK_USER_TOKEN or SLACK_BOT_TOKEN) or use --user-token / --bot-token")

    if args.quiet:
        loglevel = 100
    elif args.debug:
        loglevel = logging.DEBUG
    elif args.verbose:
        loglevel = logging.INFO
    else:
        loglevel = logging.WARNING

    if args.log:
        logging.basicConfig(filename=args.log, filemode="a", level=loglevel)
    else:
        logging.basicConfig(level=loglevel)

    client = util.client(
        token=args.user_token,
        timeout=120,
    )

    msgObj = json.load(args.msg)

    if args.users:
        userObjects = util.get_userObj(args.users)
        changedMembers = sendMsg(members=userObjects, msgObj=msgObj, allmembers=args.all)
    elif not sys.stdin.isatty():
        if args.immediate:
            changedMembers = []
            for line in sys.stdin:
                userObject = util.get_userObj(line)
                changedMembers += sendMsg(members=[userObject], msgObj=msgObj, allmembers=args.all)
        else:
            userObjects = util.get_userObj(sys.stdin)
            changedMembers = sendMsg(members=userObjects, msgObj=msgObj, allmembers=args.all)
    count_output = 0
    for changedMember in changedMembers:
        if args.csv:
            if count_output == 0 and not args.no_headings:
                util.write_csv_heading(
                    fieldmap=util.FIELDMAP_USER,
                    file=args.outfile
                )
            util.write_csv(
                fieldmap=util.FIELDMAP_USER,
                dataObj=changedMember,
                file=args.outfile
            )
        else:
            print(json.dumps(changedMember), file=args.outfile)
