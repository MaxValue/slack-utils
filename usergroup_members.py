#!/usr/bin/env python3
# coding: utf-8

import argparse, logging, requests, json, os, sys
import slack_utils as util

parser = argparse.ArgumentParser(argument_default=False, description="Gets all members of an usergroup.")
parser.add_argument("--verbose", "-v", action="count", default=0, help="Turn on verbose mode.")
parser.add_argument("--quiet", "-q", action="store_true", help="Only return data but no log messages.")
parser.add_argument("--log", help="Logfile path. If omitted, stdout is used.")
parser.add_argument("--debug", "-d", action="store_true", help="Log all messages including debug.")
parser.add_argument("--csv", action="store_true", help="Return data as CSV row.")
parser.add_argument("--no-headings", action="store_true", help="Do not print CSV header.")
parser.add_argument("--outfile", type=argparse.FileType("w", encoding="utf-8"), default=sys.stdout, help="The file in which to save the export data to.")
parser.add_argument("--user-token", default=os.environ.get("SLACK_USER_TOKEN", False), help="The user authorization token. An environment variable (SLACK_USER_TOKEN) is recommended though.")
parser.add_argument("--bot-token", default=os.environ.get("SLACK_BOT_TOKEN", False), help="The bot authorization token. An environment variable (SLACK_BOT_TOKEN) is recommended though.")
parserGroup_editMode = parser.add_mutually_exclusive_group(required=True)
parserGroup_editMode.add_argument("--get", action="store_true", help="Get existing members of this group.")
parserGroup_editMode.add_argument("--add", action="store_true", help="Add the given users to the group, keeping existing members.")
parserGroup_editMode.add_argument("--set", action="store_true", help="Replace existing members with the given users.")
parserGroup_editMode.add_argument("--substract", action="store_true", help="Remove the given members from the group.")
parser.add_argument("group", help="Either the handle of the usergroup or the id or a usergroup object.")
parser.add_argument("users", nargs="*", default=sys.stdin, help="The user IDs or user objects to add.")
args = parser.parse_args()

if not args.user_token or not args.bot_token:
    parser.error("No token provided! Provide an env var (SLACK_USER_TOKEN or SLACK_BOT_TOKEN) or use --user-token / --bot-token")

if args.quiet:
    loglevel = 100
elif args.debug:
    loglevel = logging.DEBUG
elif args.verbose:
    loglevel = logging.INFO
else:
    loglevel = logging.WARNING

if args.log:
    logging.basicConfig(filename=args.log, filemode="a", level=loglevel)
else:
    logging.basicConfig(level=loglevel)

client = util.client(token=args.user_token)

try:
    groupObj = json.loads(args.group)
    if type(groupObj) == str:
        usergroupID = groupObj
    else:
        usergroupID = groupObj.get("id", "")
except json.decoder.JSONDecodeError:
    usergroupID = args.group.strip()

if usergroupID.startswith("@"):
    logging.debug(f"Getting all usergroups.")
    all_usergroups = util.return_all(
                    client.usergroups_list,
                    datakey="usergroups",
                    include_disabled=True,
                    include_users=True
                )
    for usergroup in all_usergroups:
        if usergroup.get("handle", "") == usergroupID[1:]:
            logging.info(f"Found usergroup with handle {usergroupID}!")
            groupObj = usergroup
            usergroupID = groupObj.get("id", "")
            members = groupObj.get("users", [])
            break
    else:
        logging.error(f"Could not find usergroup with handel {usergroupID}")
        exit()

all_members = util.return_all(client.usergroups_users_list, datakey="users", usergroup=usergroupID)
if args.get:
    count_output = 0
    for member in all_members:
        if args.csv:
            if count_output == 0 and not args.no_headings:
                util.write_csv_heading(
                    fieldmap=util.FIELDMAP_USER,
                    file=args.outfile
                )
            util.write_csv(
                fieldmap=util.FIELDMAP_USER,
                dataObj=member,
                file=args.outfile
            )
        else:
            print(json.dumps(member, separators=(',', ':')), file=args.outfile)
        count_output += 1
    exit()
elif args.set:
    desiredMembers = set()
else:
    desiredMembers = set(all_members)

try:
    for user in args.users:
        try:
            userObj = json.loads(user)
            if type(userObj) == str:
                userID = userObj
                userObj = {
                    "id": userID
                }
            else:
                userID = userObj.get("id", "")
        except json.decoder.JSONDecodeError:
            userID = user.strip()
            userObj = {
                "id": userID
            }
        if util.re_userID.match(userID):
            if args.set or args.add:
                desiredMembers.add(userID)
            elif args.substract:
                desiredMembers.remove(userID)
        else:
            logging.error(f"{userID} is not a valid user id!")
except KeyboardInterrupt:
    exit()

logging.debug(f"The following users will be added to usergroup {usergroupID!r}: {desiredMembers}")

results = util.return_one(
    client.usergroups_users_update,
    usergroup=usergroupID,
    users=",".join(desiredMembers)
)
