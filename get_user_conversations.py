#!/usr/bin/env python3
# coding: utf-8
#

import argparse, logging, requests, json, os, sys
import slack_utils as util

parser = argparse.ArgumentParser(argument_default=False, description="Gets all conversations a user is part of.")
parser.add_argument("--verbose", "-v", action="count", default=0, help="Turn on verbose mode.")
parser.add_argument("--quiet", "-q", action="store_true", help="Only return data but no log messages.")
parser.add_argument("--log", help="Logfile path. If omitted, stdout is used.")
parser.add_argument("--debug", "-d", action="store_true", help="Log all messages including debug.")
parser.add_argument("--csv", action="store_true", help="Return data as CSV row.")
parser.add_argument("--no-headings", action="store_true", help="Do not print CSV header.")
parser.add_argument("--outfile", type=argparse.FileType("w", encoding="utf-8"), default=sys.stdout, help="The file in which to save the export data to.")
parser.add_argument("--user-token", default=os.environ.get("SLACK_USER_TOKEN", False), help="The user authorization token. An environment variable (SLACK_USER_TOKEN) is recommended though.")
parser.add_argument("--bot-token", default=os.environ.get("SLACK_BOT_TOKEN", False), help="The bot authorization token. An environment variable (SLACK_BOT_TOKEN) is recommended though.")
parser.add_argument("user", nargs="*", default=sys.stdin, help="The ID of the targeted user or the user object.")
args = parser.parse_args()

if not args.user_token or not args.bot_token:
    parser.error("No token provided! Provide an env var (SLACK_USER_TOKEN or SLACK_BOT_TOKEN) or use --user-token / --bot-token")

if args.quiet:
    loglevel = 100
elif args.debug:
    loglevel = logging.DEBUG
elif args.verbose:
    loglevel = logging.INFO
else:
    loglevel = logging.WARNING

if args.log:
    logging.basicConfig(filename=args.log, filemode="a", level=loglevel)
else:
    logging.basicConfig(level=loglevel)

client = util.client(token=args.user_token)

for user in args.user:
    try:
        userObj = json.loads(user)
        if type(userObj) == str:
            userID = userObj
        else:
            userID = userObj.get("id", "")
    except json.decoder.JSONDecodeError:
        userID = user.strip()
        userObj = {
            "id": userID
        }
    if not util.re_userID.match(userID):
        parser.error(f"{userID} is not a valid user id!")
    break

user_conversations = []

logging.debug("Getting all conversations.")
all_conversations = util.return_all(
    client.conversations_list,
    datakey="channels",
    limit=1000,
    types="public_channel,private_channel,mpim,im"
)

logging.debug(f"Finding user {userID} in conversations.")
for conversation in all_conversations:
    if not conversation.get("is_archived", False):
        users = util.return_all(client.conversations_members, datakey="members", channel=conversation["id"])
        for user in users:
            if userID == user:
                logging.info(f"Found user {userID} in conversation {conversation['id']}({conversation.get('name', 'Unnamed')}).")
                user_conversations.append(conversation)
                break

logging.info(f"Found user {userID} in {len(user_conversations)} conversations.")

count_output = 0
for user_conversation in user_conversations:
    if args.csv:
        if count_output == 0 and not args.no_headings:
            util.write_csv_heading(
                fieldmap=util.FIELDMAP_CHANNEL,
                file=args.outfile
            )
        util.write_csv(
            fieldmap=util.FIELDMAP_CHANNEL,
            dataObj=user_conversation,
            file=args.outfile
        )
    else:
        print(user_conversation)
    count_output += 1
