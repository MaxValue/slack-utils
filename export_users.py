#!/usr/bin/env python3
# coding: utf-8
#

import argparse
import dateparser
import datetime
import logging
import os
import pandas
import slack_utils as util
import sys
import numpy

parser = argparse.ArgumentParser(argument_default=False, description="Exports all users of a workspace.")
parser.add_argument("--verbose", "-v", action="count", default=0, help="Turn on verbose mode.")
parser.add_argument("--quiet", "-q", action="store_true", help="Only return data but no log messages.")
parser.add_argument("--log", help="Logfile path. If omitted, stdout is used.")
parser.add_argument("--debug", "-d", action="store_true", help="Log all messages including debug.")
parser.add_argument("--csv", action="store_true", help="Return data as CSV row.")
parser.add_argument("--no-headings", action="store_true", help="Do not print CSV header.")
parser.add_argument("--include-fields", action="store_true", help="Include custom user profile fields.")
parser.add_argument("--access-logs-file", help="If provided it skips the log download process and uses this file.")
parser.add_argument("--analytics", help="If you want to include analytics data (e.g.: no. of messages sent) provide an analytics export here (get it from https://YOURSLACKINSTANCE.slack.com/admin/stats#members).")
parser.add_argument("--access-logs", action="store_true", help="Whether you want to include access data (e.g.: when the user last logged in).")
parser.add_argument("--outfile", type=argparse.FileType("w", encoding="utf-8"), default=sys.stdout, help="The file in which to save the export data to.")
parser.add_argument("--user-token", default=os.environ.get("SLACK_USER_TOKEN", False), help="The user authorization token. An environment variable (SLACK_USER_TOKEN) is recommended though.")
parser.add_argument("--bot-token", default=os.environ.get("SLACK_BOT_TOKEN", False), help="The bot authorization token. An environment variable (SLACK_BOT_TOKEN) is recommended though.")
args = parser.parse_args()

if not args.user_token or not args.bot_token:
    parser.error("No token provided! Provide an env var (SLACK_USER_TOKEN or SLACK_BOT_TOKEN) or use --user-token / --bot-token")

if args.quiet:
    loglevel = 100
elif args.debug:
    loglevel = logging.DEBUG
elif args.verbose:
    loglevel = logging.INFO
else:
    loglevel = logging.WARNING

if args.log:
    logging.basicConfig(filename=args.log, filemode="a", level=loglevel)
else:
    logging.basicConfig(level=loglevel)
logger = logging.getLogger(__name__)

client = util.client(token=args.user_token)

NOW = datetime.datetime.now()

teamSlug = util._teamInfo.get("domain", "")
logger.info(f"Team is {teamSlug}")

logger.info("Getting members list")
members = pandas.json_normalize(
    data = util.return_all(
        client.users_list,
        datakey="members",
        limit=200,
    ),
    max_level = 2,
)

if args.access_logs:
    if args.access_logs_file:
        logger.info("Parsing access log file")
        accessLogs = pandas.read_csv(
            filepath_or_buffer=args.access_logs_file,
            delimiter=";",
            # index_col=False,
            lineterminator="\n",
            encoding="utf-8",
            parse_dates=[
                "date_first",
                "date_last",
            ],
        )
    else:
        logger.info("Getting fresh access logs")
        params = {
            "datakey": "logins",
            "count": 1000,
        }
        before = NOW.timestamp()
        accessLogs_raw = []
        while before > ( NOW - datetime.timedelta(days=32*6) ).timestamp():
            accessLogs_raw += util.return_all(
                client.team_accessLogs,
                **params,
            )
            before = accessLogs_raw[-1]["date_first"]
            params["before"] = before

        accessLogs = pandas.DataFrame(accessLogs_raw)
        raw_data_time = int(NOW.timestamp())
        raw_data_filename = f"{teamSlug}_accesslogs_{raw_data_time}.csv"
        logger.info(f"Saving raw access log data to {raw_data_filename}")
        accessLogs.to_csv(
            path_or_buf=raw_data_filename,
            sep=";",
            index=False,
            encoding="utf-8",
            line_terminator="\n",
        )
        accessLogs.set_index(
            keys="user_id",
            inplace=True,
        )

    logger.info(f"Calculating first and last logins for users")
    accessLogs.drop(
        labels=[
            "username",
            "count",
            "ip",
            "user_agent",
            "isp",
            "country",
            "region",
        ],
        axis="columns",
        inplace=True,
    )
    accessLogs_final = accessLogs.groupby(
        ["user_id"]
    ).aggregate(
        {
            "date_first": numpy.min,
            "date_last": numpy.max,
        },
    )
    logger.info(f"Adding login times to member data")
    membersFinal = members.merge(
        right=accessLogs_final,
        how="outer",
        left_on="id",
        right_on="user_id",
        suffixes=(
            " (members)",
            " (accessLogs)",
        ),
        validate="one_to_one",
    )

if args.analytics:
    logger.info(f"Parsing analytics export")
    analytics_file = pandas.read_csv(
        args.analytics,
        parse_dates=[
            "Account created (UTC)",
            "Claimed Date (UTC)",
            "Deactivated date (UTC)",
            "Last active (UTC)",
            "Last active (Desktop) (UTC)",
            "Last active (Android) (UTC)",
            "Last active (iOS) (UTC)",
        ],
    )
    membersFinal = membersFinal.merge(
        right=analytics_file,
        how="outer",
        left_on="id",
        right_on="User ID",
        suffixes=(
            " (members)",
            " (analytics)",
        ),
        validate="one_to_one"
    )
    membersFinal.drop(
        labels=[
            "User ID",
        ],
        axis="columns",
        inplace=True,
    )

logger.info(f"Returning results")
count_output = 0
if args.csv:
    membersFinal.to_csv(
        path_or_buf=args.outfile,
        sep=";",
        header=(False if args.no_headings else True),
        index=False,
        # index_label=
        mode="a",
        encoding="utf-8",
        line_terminator="\n",
    )
else:
    print(member, file=args.outfile) # FIXME

logger.info(f"Exported {len(members)} members from {teamSlug}.slack.com")
