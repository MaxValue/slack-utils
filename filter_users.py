#!/usr/bin/env python3
# coding: utf-8
#

import argparse, logging, os, sys, json
import slack_utils as util

parser = argparse.ArgumentParser(argument_default=False, description="Finds users inside of a workspace.")
# parser.add_argument("--last-active", type=int, help="Filter for users last active before this time (in EPOCH).")
parser.add_argument("--name", help="Filter for users with their names containing this string.")
parser.add_argument("--display-name", help="Filter for users with their display names containing this string.")
parser.add_argument("--handle", help="Filter for users with their @-handles containing this string.")
parser.add_argument("--ignore-case", "-i", action="store_true", help="Ignore case distinctions, so that characters that differ only in case match each other.")
parser.add_argument("--count", "-c", type=int, help="Only return matches if this number of users is found.")
parser.add_argument("--exact", action="store_true", help="Only exact matches. Does not apply to --name.")
parser.add_argument("--id", help="Filter for users which have this Slack id.")
parser.add_argument("--email", help="Filter for users which have this e-mail address.")
parser.add_argument("--verbose", "-v", action="count", default=0, help="Turn on verbose mode.")
parser.add_argument("--quiet", "-q", action="store_true", help="Only return data but no log messages.")
parser.add_argument("--log", help="Logfile path. If omitted, stdout is used.")
parser.add_argument("--debug", "-d", action="store_true", help="Log all messages including debug.")
parser.add_argument("--csv", action="store_true", help="Return data as CSV row.")
parser.add_argument("--no-headings", action="store_true", help="Do not print CSV header.")
parser.add_argument("--outfile", type=argparse.FileType("w", encoding="utf-8"), default=sys.stdout, help="The file in which to save the export data to.")
parser.add_argument("--user-token", default=os.environ.get("SLACK_USER_TOKEN", False), help="The user authorization token. An environment variable (SLACK_USER_TOKEN) is recommended though.")
parser.add_argument("--bot-token", default=os.environ.get("SLACK_BOT_TOKEN", False), help="The bot authorization token. An environment variable (SLACK_BOT_TOKEN) is recommended though.")
args = parser.parse_args()

if not args.user_token or not args.bot_token:
    parser.error("No token provided! Provide an env var (SLACK_USER_TOKEN or SLACK_BOT_TOKEN) or use --user-token / --bot-token")

if args.quiet:
    loglevel = 100
elif args.debug:
    loglevel = logging.DEBUG
elif args.verbose:
    loglevel = logging.INFO
else:
    loglevel = logging.WARNING

if args.log:
    logging.basicConfig(filename=args.log, filemode="a", level=loglevel)
else:
    logging.basicConfig(level=loglevel)

client = util.client(token=args.user_token)

found_users = []
count = 0

logging.debug("Getting all users.")
all_users = util.return_all(client.users_list, datakey="members")
logging.debug(f"Finding user in user list.")
for user in all_users:
    if args.count and count > args.count:
        break
    match = False
    if args.id:
        if args.exact:
            if user.get("id", "").lower() == args.id.lower():
                logging.debug(f"Found user by ID {args.id}!")
                match = True
            else:
                match = False
        else:
            if user.get("id", "").lower() in args.id.lower():
                logging.debug(f"Found user by ID {args.id}!")
                match = True
            else:
                match = False
    if args.email:
        if args.exact:
            if util.jmes("profile.email", user, "").lower() == args.email.lower():
                logging.debug(f"Found user by email {args.email}!")
                match = True
            else:
                match = False
        else:
            if util.jmes("profile.email", user, "").lower() in args.email.lower():
                logging.debug(f"Found user by email {args.email}!")
                match = True
            else:
                match = False
    if args.name:
        namevalues = [
            user.get("name", ""),
            user.get("real_name", ""),
            util.jmes("profile.real_name", user, ""),
            util.jmes("profile.real_name_normalized", user, ""),
            util.jmes("profile.display_name", user, ""),
            util.jmes("profile.display_name_normalized", user, ""),
            util.jmes("profile.first_name", user, ""),
            util.jmes("profile.last_name", user, ""),
        ]
        for namevalue in namevalues:
            if args.ignore_case:
                if args.name.lower() in namevalue.lower():
                    logging.debug(f"Found user by name {args.name}!")
                    match = True
                    break
            else:
                if args.name in namevalue:
                    logging.debug(f"Found user by name {args.name}!")
                    match = True
                    break
        else:
            match = False
    if args.display_name:
        user_displayName = util.jmes("profile.display_name", user, "")
        if args.exact:
            if args.display_name.lower() == user_displayName.lower():
                logging.debug(f"Display name {user_displayName!r} matches given string {args.display_name!r} exactly.")
                match = True
            else:
                match = False
        else:
            if args.display_name.lower() in user_displayName.lower():
                logging.debug(f"Display name {user_displayName!r} contains given string {args.display_name!r}.")
                match = True
            else:
                match = False
    if args.handle:
        user_handle = util.jmes(
            path="profile.display_name",
            dataObj=user,
            default=util.jmes(
                path="profile.real_name",
                dataObj=user,
                default=""
            )
        )
        if args.exact:
            if args.handle.lower() == user_handle.lower():
                logging.debug(f"Handle {user_handle!r} matches given string {args.handle!r} exactly.")
                match = True
            else:
                match = False
        else:
            if args.handle.lower() in user_handle.lower():
                logging.debug(f"Handle {user_handle!r} contains given string {args.handle!r}.")
                match = True
            else:
                match = False
    if match:
        count += 1
        found_users.append(user)

returnResults = False
if args.count:
    if count == args.count:
        returnResults = True
        logging.info(f"Found {len(found_users)} matching users (exactly {args.count} were found).")
    else:
        returnResults = False
        logging.debug(f"Not returning any matches because found number of users does not equal given user count.")
else:
    returnResults = True
    logging.info(f"Found {len(found_users)} matching users.")

if returnResults:
    count_output = 0
    for found_user in found_users:
        if args.csv:
            if count_output == 0 and not args.no_headings:
                util.write_csv_heading(
                    fieldmap=util.FIELDMAP_USER,
                    file=args.outfile
                )
            util.write_csv(
                fieldmap=util.FIELDMAP_USER,
                dataObj=found_user,
                file=args.outfile
            )
        else:
            print(json.dumps(found_user, separators=(',', ':')), file=args.outfile)
        count_output += 1
else:
    exit(1)
